# CHANGELOG PROJEKTU "RetroBaza.pl"

## Wersja v.0.1 - 22.03.2019 r.
 * Pierwsza wersja projektu
 * Opcja logowania
 * Opcja rejestracji
 * Podstawowa wersja szablonu strony
## Wersja v.0.2 - 29.03.2019 r.(Uszkodzona!)
 * Oczyszczenie repozytorium ze zbędnych plików
 * Dodano zakładkę forum
 * Dodano możliwość tworzenia tematów
 * Dodano możliwość tworzenia postów na forum
 * Zmiana szablonu strony na wykorzystujący bibliotekę BootStrap
 * Zwiększono czytelność zakładek wylogowywania i rejestracji
## Wersja v.0.2.1 - 05.04.2019 r.
 * Przywrócenie do stanu v.0.2
 * Rozszerzenie modelu użytkownika i kategorii o pola zdjęciowe
 * Wyświetlanie miniatur w opisach kategorii
## Wersja v.0.3 - 12.04.2019 r.
 * Wyświetlanie awatarów użytkowników
 * Ostateczna wersja modelu plików
 * Testy jednostkowe forum
 * Podstawowa wersja uploadu plików
 * Wyświetlanie biblioteki plików
## Wersja v.0.3.5 - 16.04.2019 r.
 * Poprawa wyświetlania listy plików
## Wersja v.0.5 - 26.04.2019 r.
 * Poprawiono formularz wgrywania plików
 * Możliwość dodawania nowych marek, modeli i typów urządzeń z poziomu uploadu
 * Początek dodawania opcji przycinania zdjęć
## Wersja v.0.6 - 10.05.2019 r.
 * Panel edycji użytkownika
 * Dokończenie mechanizmu przycinania zdjęć
 * Początek naprawy awatarów (poprawne wyświetlanie placeholderów)
## Wersja v.0.7 - 17.05.2019 r.
 * Naprawa panelu edycji użytkownika
 * Testy jednostkowe edycji plików i użytkownika
## Wersja v.0.8 - 24.05.2019 r.
 * Dodanie panelu administratorskiego
 * Pojawienie się rang użytkowników
 * Możliwość blokady użytkowników
 * Początek prac nad modelem REST API (wyświetlanie pojedynczych rekordów)
## Wersja v.0.9 - 31.05.2019 r.
 * Dodanie panelu resetowania hasła zintegrowanego z usługą Mailgun (tylko 2 adresy)
 * Zmiana sposobu wstawiania nowych postów (dynamiczne wstawianie nowych wpisów)
 * Edycja postów z poziomu tematu
 * Możliwość wyszukiwania i paginacji dla paneli administatorskich