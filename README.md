# RetroBaza.pl

Projekt strony www oparty na pythonowym frameworku Django.

## Uruchomienie lokalne

Aby uruchomić lokalnie projekt należy w pierwszej kolejności mieć zainstalowane następujące komponenty:

```
- Python 
- VirtualEnv (należy go zainstalować w katalogu przechowywania projektu)
- Django (podobnie)
```
### Instalacja i pierwsza konfiguracja komponentów

Aby zainstalować Pythona, należy go pobrać stąd:
```
https://www.python.org/downloads/
```
pamiętając o tym, żeby miał dostęp do PATH.

Następnie odpalamy PowerShell (rekomendowane) lub wiersz poleceń i wpisujemy następujące komendy:
```
pip install virtualenv
pip install virtualenvwrapper
```
aby zainstalować VirtualEnv.

W następnym kroku pamiętamy, aby przejść do katalogu, w którym ma być nasze wirtualne środowisko i wpisujemy komendę:
```
virtualenv <nazwa-srodowiska>
```
gdzie zamiast <nazwa-srodowiska>, wpisujemy nazwę swojego środowiska. Potem je odpalamy wpisując komendę:
```
env/scripts/activate
```
Jeśli w wierszu poleceń PowerShell jest widoczny taki zapis:
```
(nazwa-srodowiska) <Lokalizacja wirtualnego srodowiska>
```
To oznacza, że VirtualEnv został prawidłowo skonfigurowany, i możemy przejść do instalacji Django w tym folderze poprzez komendę:
```
pip install django
```
Następnie tworzymy Hub, czyli domyślne miejsce konfiguracji projektu:
```
django-admin startproject <nazwa projektu>
```
Powershell wtedy automatycznie przejdzie do folderu projektu. Kopiujemy do niego nasze foldery z aplikacją i wykonujemy migrację:
```
python manage.py migrate
```
W następnej kolejności tworzymy konto superusera (głównego administratora strony):
```
python manage.py createsuperuser
```
Podłączamy aplikację do ustawień projektu dodając do pliku settings.py we wskazanym punkcie:
```
INSTALLED_APPS = [
    ...
	'retrobaza', <- to dodajemy
]
```
Na końcu odpalamy serwer, w celu przetestowania działania strony:
```
python manage.py runserver
```
### Kolejne uruchomienia

Aby uruchomić już skonfigurowaną aplikację Django należy wykonać następujący skrypt:
```
<Przejście do katalogu środowiska>
env/scripts/activate
<Przejście do katalogu projektu>
python manage.py runserver
```
## Umieszczenie na serwerze

W.I.P. - przewidywana data koniec 05.2019

## Zbudowane przy pomocy:
* [Django](https://docs.djangoproject.com/en/2.1/) - Użyty framework do back-end development
* [Bootstrap](https://getbootstrap.com/docs/4.3/getting-started/introduction/)- Framework użyty do front-end development

## Zmiany

Sprawdź [Changelog.md]

## Autorzy
* **Krzysztof Piskorz** 

## Pokaz możliwości aplikacji
![Główny ekran](screenshots/main.png)
Główny ekran aplikacji
![Biblioteka - ekran główny](screenshots/Filelibrary.png)
Widok biblioteki urządzeń
![Biblioteka - widok pojedynczego urządzenia](screenshots/fileList.png)
Widok biblioteki plików dostępnych dla urządzenia
![Biblioteka - widok dodawania pliku](screenshots/newFile.png)
Widok dodawania nowego pliku
![Użytkownik - panel ustawień](screenshots/usercontrol.png)
Widok panelu sterowania użytkownika
![Użytkownik - panel administratora](screenshots/Adminpanel.png)
Widok panelu administracyjnego
![Forum - widok kategorii](screenshots/forumList.png)
Widok kategorii forum
![Forum - widok tematów](screenshots/topicList.png)
Widok tematów w danej kategorii forum
![Forum - widok tematu](screenshots/topicDemo.png)
Widok tematu