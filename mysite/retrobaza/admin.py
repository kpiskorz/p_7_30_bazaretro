from django.contrib import admin
from .models import Brand, ModelName, DeviceType, Category, Topic, Post
# Register your models here.
admin.site.register(Brand)
admin.site.register(ModelName)
admin.site.register(DeviceType)
admin.site.register(Category)
admin.site.register(Topic)
admin.site.register(Post)