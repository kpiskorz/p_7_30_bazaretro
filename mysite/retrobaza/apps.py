from django.apps import AppConfig


class RetrobazaConfig(AppConfig):
    name = 'retrobaza'
