from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth.models import User
from .models import Brand, ModelName, DeviceType, Category, Topic, Post, File, Profile

class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=False, help_text='Pole opcjonalne')
    last_name = forms.CharField(max_length=30, required=False, help_text='Pole opcjonalne')
    email = forms.EmailField(max_length=254, help_text='Pole wymagane')
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', )

class AddTopic(forms.ModelForm):
    titleCat = forms.ModelChoiceField(queryset=Category.objects.all(),to_field_name='catName')
    class Meta:
        model = Topic
        fields = ('titleName', 'titleCat',)

class AddPost(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('postData',)

class AddFile(forms.ModelForm):
    class Meta:
        model = File
        fields = ('fileName', 'fileDesc', 'brand', 'model', 'deviceType', 'file', )
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['model'].queryset = ModelName.objects.none()
        if 'brand' in self.data:
                try:
                    brand = int(self.data.get('brand'))
                    self.fields['model'].queryset = ModelName.objects.filter(brand = brand).order_by('name')
                except (ValueError, TypeError):
                    pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            self.fields['model'].queryset = self.instance.brand.modelname_set.order_by('modelName')

class AddModel(forms.ModelForm):
    class Meta:
        model = ModelName
        fields = ('modelName', 'brand',)

class AddBrand(forms.ModelForm):
    class Meta:
        model = Brand
        fields = ('brandName',)

class AddType(forms.ModelForm):
    class Meta:
        model = DeviceType
        fields = ('deviceTypeName',)

class EditUserForm(UserChangeForm):
    class Meta:
        model = User
        fields = (
            'first_name',
            'last_name',
            'email',
            'password'
        )

class PhotoForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = (
            'avatar',
        )

class DeleteConfirm(forms.Form):
    userId = forms.IntegerField(widget=forms.HiddenInput())

class ChangeUserType(UserChangeForm):
    class Meta:
        model = User
        fields = (
            'is_staff',
            'is_superuser',
            'password'
        )