from rest_framework import serializers
from .models import Brand, DeviceType, Category, ModelName, File, Topic, Post

class BrandSerializer(serializers.ModelSerializer):
    class Meta:
        model = Brand
        fields = ('id','brandName', 'brandSlug')

class DeviceTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = DeviceType
        fields = ('id','deviceTypeName', 'deviceSlug')

class CategorySerializer(serializers.ModelSerializer):
    catPic_url = serializers.ImageField(max_length=None, use_url=True, allow_null=True, required=False)
    class Meta:
        model = Category
        fields = ('id', 'catName', 'catDesc', 'catPic_url')

class ModelNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = ModelName
        fields = ('id', 'modelName', 'brand', 'modelSlug')

class TopicSerializer(serializers.ModelSerializer):
    class Meta:
        model = Topic
        fields = '__all__'

class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = '__all__'

class FileSerializer(serializers.ModelSerializer):
    file_url = serializers.FileField(max_length=None, use_url=True, allow_null=True, required=False)
    class Meta:
        model = File
        fields = ('id', 'fileName', 'fileDesc', 'uploader', 'model', 'deviceType', 'file_url')
