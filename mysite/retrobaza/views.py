from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import TemplateView
from django.urls import reverse_lazy
from django.views import generic
from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm
from django.utils import timezone
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth import update_session_auth_hash
from rest_framework.generics import DestroyAPIView, UpdateAPIView, RetrieveAPIView, ListCreateAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from django.db.models import Q

from .serializers import (
    BrandSerializer,
    DeviceTypeSerializer,
    CategorySerializer,
    ModelNameSerializer,
    TopicSerializer,
    PostSerializer,
    FileSerializer
)

from .forms import (
    SignUpForm,
    AddPost, 
    AddTopic, 
    AddBrand, 
    AddModel, 
    AddType, 
    AddFile, 
    EditUserForm,
    DeleteConfirm,
    PhotoForm,
    ChangeUserType
)

from .models import (
    User,
    Brand, 
    ModelName, 
    DeviceType, 
    Category, 
    Topic, 
    Post, 
    Profile, 
    File,
)
# Create your views here.
class RBIndex(TemplateView):
    template_name = 'index.html'

class RBContact(TemplateView):
    template_name = 'contact.html'

class RBPartners(TemplateView):
    template_name = 'partners.html'

#Widok rejestracji
def RBSignUp(request):
    if not request.user.is_authenticated:
        if request.method == 'POST':
            form = SignUpForm(request.POST, request.FILES)
            if form.is_valid():
                user = form.save()
                user.refresh_from_db()
                user.profile.save()
                raw_password = form.cleaned_data.get('password1')
                user = authenticate(username=user.username, password=raw_password)
                login(request, user)
                return redirect('retrobaza-index')
        else:
            form = SignUpForm()
        return render(request, 'signup.html', {'form': form})
    else:
        return redirect('retrobaza-index')

#Widok kategorii forum
def RBFCategories(request):
    categories = Category.objects.all()
    return render(request, 'forum.html', {'categories': categories})

#Widok tematów w kategorii
def RBFTopics(request, slug):
    category = get_object_or_404(Category, catSlug = slug)
    topics_list = Topic.objects.filter(titleCat = category.pk)
    paginator = Paginator(topics_list, 20)
    page = request.GET.get('page', 1)
    try:
        topics = paginator.page(page)
    except PageNotAnInteger:
        topics = paginator.page(1)
    except EmptyPage:
        topics = paginator.page(paginator.num_pages)
    return render(request, 'category.html', {'category': category, 'topics':topics})

#Widok tematu
def RBFTopic(request, slug):
    topic = get_object_or_404(Topic, titleSlug = slug)
    posts_list = Post.objects.filter(topic = topic.pk)
    paginator = Paginator(posts_list, 20)
    page = request.GET.get('page', 1)
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)
    return render(request, 'topic.html', {'topic': topic, 'posts':posts})

#Tworzenie nowego tematu (Wymagane logowanie)
@login_required(redirect_field_name='login')
def RBFNTopic(request):
    if request.method == 'POST':
        add_topic = AddTopic(request.POST)
        add_post = AddPost(request.POST)
        if add_topic.is_valid() and add_post.is_valid():
            topic = add_topic.save(False)
            topic.author = request.user.profile
            topic.created_date = timezone.now()
            topic.save()
            post = add_post.save(False)
            post.author = request.user.profile
            post.created_date = timezone.now()
            post.topic = topic
            post.save()
            return redirect('retrobaza-forum')
    else:
        add_topic = AddTopic()
        add_post = AddPost()
    return render(request, 'addtopic.html', {'add_topic': add_topic, 'add_post':add_post})

#Tworzenie nowego posta (Wymagane logowanie)
@login_required(redirect_field_name='login')
def RBFNPost(request, pk):
    post = get_object_or_404(Post, pk = pk)
    topic = get_object_or_404(Topic, pk = post.topic.pk)
    add_post = AddPost(request.POST, instance=post)
    if add_post.is_valid():
        post = add_post.save(False)
        post.author = request.user.profile
        post.created_date = timezone.now()
        post.topic = topic
        post.save()
        return redirect('topic-details', slug = post.topic.titleSlug)
    else:
        add_post = AddPost()
    return render(request, 'addtopic.html', {'topic': topic, 'add_post':add_post})
    
#Widok główny biblioteki plików
def RBBMain(request):
    brands = Brand.objects.all()
    deviceType = DeviceType.objects.all()
    return render(request, 'libraryOptions.html', {'brands': brands, 'types': deviceType})

#Widok biblioteki dla danej marki
def RBBBrand(request, slug):
    brand = get_object_or_404(Brand, brandSlug = slug)
    model_list = ModelName.objects.filter(brand = brand.pk)
    paginator = Paginator(model_list, 20)
    page = request.GET.get('page', 1)
    try:
        models = paginator.page(page)
    except PageNotAnInteger:
        models = paginator.page(1)
    except EmptyPage:
        models = paginator.page(paginator.num_pages)
    return render(request, 'libraryBrands.html', {'brand': brand, 'models':models})

#Widok biblioteki dla danego modelu
def RBBModel(request, brandSlug, modelSlug):
    brand = get_object_or_404(Brand, brandSlug = brandSlug)
    model = get_object_or_404(ModelName, modelSlug = modelSlug)
    file_list = File.objects.filter(model = model.pk)
    paginator = Paginator(file_list, 20)
    page = request.GET.get('page', 1)
    try:
        files = paginator.page(page)
    except PageNotAnInteger:
        files = paginator.page(1)
    except EmptyPage:
        files = paginator.page(paginator.num_pages)
    return render(request, 'libraryModel.html',  {'brand':brand, 'model': model, 'files':files})

#Widok biblioteki dla danego typu urządzeń
def RBBType(request, slug):
    dType = get_object_or_404(DeviceType, deviceSlug = slug)
    file_list = File.objects.filter(deviceType = dType.pk)
    paginator = Paginator(file_list, 20)
    page = request.GET.get('page', 1)
    try:
        files = paginator.page(page)
    except PageNotAnInteger:
        files = paginator.page(1)
    except EmptyPage:
        files = paginator.page(paginator.num_pages)
    return render(request, 'libraryTypes.html', {'type': dType, 'files':files})

#Dodanie nowego pliku do biblioteki (wymaga logowania)
@login_required(redirect_field_name='login')
def RBBAdd(request):
    if request.method == 'POST':
        add_file = AddFile(request.POST, request.FILES)
        if add_file.is_valid():
            nfile = add_file.save(False)
            nfile.uploader = request.user.profile
            nfile.save()
            return redirect('retrobaza-library')
    else:
        add_file = AddFile()
    return render(request, 'libraryAddFile.html', {'add_file': add_file})

def load_models(request):
    brand_id = request.GET.get('brand')
    models = ModelName.objects.filter(brand = brand_id)
    return render(request, 'hr/models_dropdown_list_options.html', {'models': models})

def BrandCreatePopup(request):
	form = AddBrand(request.POST or None)
	if form.is_valid():
		instance = form.save()

		## Zmiana wartości "#id_brand" 
		
		return HttpResponse('<script>opener.closePopup(window, "%s", "%s", "#id_brand");</script>' % (instance.pk, instance))
	
	return render(request, "brand_form.html", {"form" : form})

def ModelCreatePopup(request):
	form = AddModel(request.POST or None)
	if form.is_valid():
		instance = form.save()
		
		return HttpResponse('<script>opener.closePopup(window, "%s", "%s", "#id_model");</script>' % (instance.pk, instance))
	
	return render(request, "model_form.html", {"form" : form})

def TypeCreatePopup(request):
	form = AddType(request.POST or None)
	if form.is_valid():
		instance = form.save()
		
		return HttpResponse('<script>opener.closePopup(window, "%s", "%s", "#id_deviceType");</script>' % (instance.pk, instance))
	
	return render(request, "type_form.html", {"form" : form})

#Panel ustawień użytkownika (wymaga zalogowania)
@login_required(redirect_field_name='login')
def EditUserData(request):
    profile = get_object_or_404(Profile, user = request.user.id)
    #Lista plików
    file_list = File.objects.filter(uploader = profile)
    paginator = Paginator(file_list, 10)
    page = request.GET.get('page1', 1)
    try:
        files = paginator.page(page)
    except PageNotAnInteger:
        files = paginator.page(1)
    except EmptyPage:
        files = paginator.page(paginator.num_pages)
    #Lista tematów
    topics_list = Topic.objects.filter(author = profile)
    paginator = Paginator(topics_list, 10)
    page = request.GET.get('page2', 1)
    try:
        topics = paginator.page(page)
    except PageNotAnInteger:
        topics = paginator.page(1)
    except EmptyPage:
        topics = paginator.page(paginator.num_pages)
    #Lista postów
    posts_list = Post.objects.filter(author = profile)
    paginator = Paginator(posts_list, 10)
    page = request.GET.get('page3', 1)
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)
    return render(request, "edituser.html", {'files': files, 'filelist': file_list, 'topics': topics, 'topicslist': topics_list,'postslist':posts_list, 'posts':posts})

#Panel edycji tematu (wymaga zalogowania)
@login_required(redirect_field_name='login')
def editTopic(request, pk):
    topic = get_object_or_404(Topic, pk=pk)
    if request.method == 'POST':
        edit_topic = AddTopic(request.POST, instance=topic)
        if edit_topic.is_valid():
            topic = edit_topic.save(False)
            topic.author = request.user.profile
            topic.created_date = timezone.now()
            topic.save()
            next = request.POST.get('next', '/')
            return HttpResponseRedirect(next)
    else:
        edit_topic = AddTopic(instance=topic)
    return render(request, 'editTopic.html', {'edit_topic': edit_topic})

#Opcja usunięcia tematu (wymaga zalogowania)
@login_required(redirect_field_name='login')
def deleteTopic(request, pk):
    if request.method == 'POST':
        topic = Topic.objects.filter(pk=pk)
        topic.delete()
    next = request.POST.get('next', '/')
    return HttpResponseRedirect(next)

#Panel edycji pliku (wymaga zalogowania)
@login_required(redirect_field_name='login')
def editFile(request, pk):
    filegot = get_object_or_404(File, pk=pk)
    if request.method == 'POST':
        edit_file = AddFile(request.POST, instance=filegot)
        if edit_file.is_valid():
            filegot = edit_file.save(False)
            filegot.uploader = request.user.profile
            filegot.save()
            next = request.POST.get('next', '/')
            return HttpResponseRedirect(next)
    else:
        edit_file = AddFile(instance=filegot)
    return render(request, 'libraryEditFile.html', {'edit_file': edit_file})

#Opcja usunięcia pliku (wymaga zalogowania)
@login_required(redirect_field_name='login')
def deleteFile(request, pk):
    if request.method == 'POST':
        fileTarget = File.objects.filter(pk=pk)
        fileTarget.delete()
    next = request.POST.get('next', '/')
    return HttpResponseRedirect(next)

#Panel edycji profilu (wymaga zalogowania)
@login_required(redirect_field_name='login')
def EditProfile(request):
    if request.method == 'POST':
        form = EditUserForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            return redirect('editUser')
    else:
        form = EditUserForm(instance=request.user)
        args = {'form': form}
        return render(request, 'editUserData.html', args)

#Panel edycji hasła (wymaga zalogowania)
@login_required(redirect_field_name='login')
def EditPassword(request):
    if request.method == 'POST':
        form = PasswordChangeForm(data=request.POST, user=request.user)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            return redirect('editUser')
        else:
            return redirect('editUserPassword')
    else:
        form = PasswordChangeForm(user=request.user)
        args = {'form': form}
        return render(request, 'editUserPassword.html', args)

#Opcja usunięcia użytkownika (wymaga zalogowania)
@login_required(redirect_field_name='login')
def DeleteUserConfirm(request):
    if request.method == 'POST':
        form = DeleteConfirm(request.POST)
        if form.is_valid():
            pk = form.cleaned_data['userId']
            profileToDelete = request.user
            profileToDelete.delete()
            return redirect('retrobaza-index')
    else:
        form = DeleteConfirm(initial = {'userId': request.user.pk})
        args = {'form': form}
        return render(request, 'deleteConfirm.html', args)

#Panel edycji awatara (wymaga zalogowania)
@login_required(redirect_field_name='login')
def EditAvatar(request):  
    if request.method == 'POST':
        photoform = PhotoForm(request.POST, instance=request.user)
        if photoform.is_valid():
            photoform.save()
            return redirect('editUser')
    else:
        photoform = PhotoForm()
    return render(request, 'editAvatar.html', {'photoform':photoform})

#Panel ustawień administratorskich (wymaga zalogowania oraz konta przynajmniej typu staff)
@login_required(redirect_field_name='login')
@user_passes_test(lambda u: u.is_superuser or u.is_staff)
def AdminOptions(request):
    profile_list = Profile.objects.all()
    profilequery = request.GET.get("fuser")
    if profilequery:
        profile_list = profile_list.filter(user__username__icontains=profilequery)
    paginator = Paginator(profile_list, 2)
    page = request.GET.get('page1',1)
    try:
        profiles = paginator.page(page)
    except PageNotAnInteger:
        profiles = paginator.page(1)
    except EmptyPage:
        profiles = paginator.page(paginator.num_pages)
    file_list = File.objects.all()
    filequery = request.GET.get("ffiles")
    if filequery:
        file_lookup = (Q(fileName__icontains=filequery)|Q(fileDesc__icontains=filequery)|Q(uploader__user__username__icontains=filequery))
        file_list = file_list.filter(file_lookup)
    paginator = Paginator(file_list, 2)
    page = request.GET.get('page2',1)
    try:
        files = paginator.page(page)
    except PageNotAnInteger:
        files = paginator.page(1)
    except EmptyPage:
        files = paginator.page(paginator.num_pages)
    topic_list = Topic.objects.all()
    topicquery = request.GET.get("ftopic")
    if topicquery:
        topic_lookup = (Q(titleName__icontains=topicquery)|Q(author__user__username__icontains=topicquery))
        topic_list = topic_list.filter(topic_lookup)
    paginator = Paginator(topic_list, 2)
    page = request.GET.get('page3',1)
    try:
        topics = paginator.page(page)
    except PageNotAnInteger:
        topics = paginator.page(1)
    except EmptyPage:
        topics = paginator.page(paginator.num_pages)
    return render(request, "adminOptions.html", {'profiles': profiles, 'files': files, 'topics':topics})

#Opcja blokady użytkownika (wymaga zalogowania oraz konta przynajmniej typu staff)
@login_required(redirect_field_name='login')
@user_passes_test(lambda u: u.is_superuser or u.is_staff)
def AdminLockUser(request, pk):
    if request.method == 'POST':
        profileToLock = get_object_or_404(Profile, pk=pk)
        if profileToLock.is_blocked:
            profileToLock.is_blocked = False
        else:
            profileToLock.is_blocked = True
        profileToLock.save()
    return redirect('adminOptions')

#Panel zmiany uprawnień (wymaga zalogowania oraz konta przynajmniej typu administrator)
@login_required(redirect_field_name='login')
@user_passes_test(lambda u: u.is_superuser)
def AdminChangeStatus(request, pk):
    usergot = get_object_or_404(User, pk=pk)
    if request.method == 'POST':
        form = ChangeUserType(request.POST, instance=usergot)
        if form.is_valid():
            form.save()
            return redirect('adminOptions')
    else:
        form = ChangeUserType(instance=usergot)
        args = {'form': form}
        return render(request, 'adminChangeType.html', args)

def NewPost(request):
    if request.method == 'POST':
        topicID = request.POST['topic']
        topic = get_object_or_404(Topic, pk=topicID)
        post = request.POST['postData']
        author = request.user.profile
        created_date = timezone.now()
        Post.objects.create(
            author = author,
            topic = topic,
            postData = post,
            created_date = created_date
        )
        return render(request, 'topic.html', {'topic':topic })

def topicentry(request):
    try:
        topicID = request.POST['topic']
    except KeyError:
        topicID = 1
    topic = get_object_or_404(Topic, pk=topicID)
    posts_list = Post.objects.filter(topic = topic)
    paginator = Paginator(posts_list, 20)
    page = request.GET.get('page', 1)
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)
    return render(request, 'topicentry.html', {'topic':topic, 'posts':posts })

#--------------------------------------------------------------
#Panel API:

class BaseManageView(APIView):
    """
    The base class for ManageViews
        A ManageView is a view which is used to dispatch the requests to the appropriate views
        This is done so that we can use one URL with different methods (GET, PUT, etc)
    """
    def dispatch(self, request, *args, **kwargs):
        if not hasattr(self, 'VIEWS_BY_METHOD'):
            raise Exception('VIEWS_BY_METHOD static dictionary variable must be defined on a ManageView class!')
        if request.method in self.VIEWS_BY_METHOD:
            return self.VIEWS_BY_METHOD[request.method]()(request, *args, **kwargs)

        return Response(status=405)
# REST API dla tabeli BRAND
class BrandList(ListCreateAPIView):
    queryset = Brand.objects.all()
    serializer_class = BrandSerializer
    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

class BrandDestroyView(DestroyAPIView):
    queryset = Brand.objects.all()
    serializer_class = BrandSerializer

class BrandUpdateView(UpdateAPIView):
    queryset = Brand.objects.all()
    serializer_class = BrandSerializer

class BrandDetailsView(RetrieveAPIView):
    queryset = Brand.objects.all()
    serializer_class = BrandSerializer

class BrandManageView(BaseManageView):
    VIEWS_BY_METHOD = {
        'DELETE': BrandDestroyView.as_view,
        'GET': BrandDetailsView.as_view,
        'PUT': BrandUpdateView.as_view,
        'PATCH': BrandUpdateView.as_view
    }

#REST API dla tabeli DeviceType
class DeviceTypeList(ListCreateAPIView):
    queryset = DeviceType.objects.all()
    serializer_class = DeviceTypeSerializer
    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

class DeviceTypeDestroyView(DestroyAPIView):
    queryset = DeviceType.objects.all()
    serializer_class = DeviceTypeSerializer

class DeviceTypeUpdateView(UpdateAPIView):
    queryset = DeviceType.objects.all()
    serializer_class = DeviceTypeSerializer

class DeviceTypeDetailsView(RetrieveAPIView):
    queryset = DeviceType.objects.all()
    serializer_class = DeviceTypeSerializer

class DeviceTypeManageView(BaseManageView):
    VIEWS_BY_METHOD = {
        'DELETE': DeviceTypeDestroyView.as_view,
        'GET': DeviceTypeDetailsView.as_view,
        'PUT': DeviceTypeUpdateView.as_view,
        'PATCH': DeviceTypeUpdateView.as_view
    }

#REST API dla tabeli File
class FileList(ListCreateAPIView):
    queryset = File.objects.all()
    serializer_class = FileSerializer
    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

class FileDestroyView(DestroyAPIView):
    queryset = File.objects.all()
    serializer_class = FileSerializer

class FileUpdateView(UpdateAPIView):
    queryset = File.objects.all()
    serializer_class = FileSerializer

class FileDetailsView(RetrieveAPIView):
    queryset = File.objects.all()
    serializer_class = FileSerializer

class FileManageView(BaseManageView):
    VIEWS_BY_METHOD = {
        'DELETE': DeviceTypeDestroyView.as_view,
        'GET': DeviceTypeDetailsView.as_view,
        'PUT': DeviceTypeUpdateView.as_view,
        'PATCH': DeviceTypeUpdateView.as_view
    }

#REST API dla tabeli ModelName
class ModelNameList(ListCreateAPIView):
    queryset = ModelName.objects.all()
    serializer_class = ModelNameSerializer
    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

class ModelNameDestroyView(DestroyAPIView):
    queryset = ModelName.objects.all()
    serializer_class = ModelNameSerializer

class ModelNameUpdateView(UpdateAPIView):
    queryset = ModelName.objects.all()
    serializer_class = ModelNameSerializer

class ModelNameDetailsView(RetrieveAPIView):
    queryset = ModelName.objects.all()
    serializer_class = ModelNameSerializer

class ModelNameManageView(BaseManageView):
    VIEWS_BY_METHOD = {
        'DELETE': ModelNameDestroyView.as_view,
        'GET': ModelNameDetailsView.as_view,
        'PUT': ModelNameUpdateView.as_view,
        'PATCH': ModelNameUpdateView.as_view
    }

#REST API dla tabeli Category
class CategoryList(ListCreateAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

class CategoryDestroyView(DestroyAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

class CategoryUpdateView(UpdateAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

class CategoryDetailsView(RetrieveAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

class CategoryManageView(BaseManageView):
    VIEWS_BY_METHOD = {
        'DELETE': CategoryDestroyView.as_view,
        'GET': CategoryDetailsView.as_view,
        'PUT': CategoryUpdateView.as_view,
        'PATCH': CategoryUpdateView.as_view
    }

#REST API dla tabeli Topic
class TopicList(ListCreateAPIView):
    queryset = Topic.objects.all()
    serializer_class = TopicSerializer
    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

class TopicDestroyView(DestroyAPIView):
    queryset = Topic.objects.all()
    serializer_class = TopicSerializer

class TopicUpdateView(UpdateAPIView):
    queryset = Topic.objects.all()
    serializer_class = TopicSerializer

class TopicDetailsView(RetrieveAPIView):
    queryset = Topic.objects.all()
    serializer_class = TopicSerializer

class TopicManageView(BaseManageView):
    VIEWS_BY_METHOD = {
        'DELETE': TopicDestroyView.as_view,
        'GET': TopicDetailsView.as_view,
        'PUT': TopicUpdateView.as_view,
        'PATCH': TopicUpdateView.as_view
    }

#REST API dla tabeli Post
class PostList(ListCreateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

class PostDestroyView(DestroyAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer

class PostUpdateView(UpdateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer

class PostDetailsView(RetrieveAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer

class PostManageView(BaseManageView):
    VIEWS_BY_METHOD = {
        'DELETE': PostDestroyView.as_view,
        'GET': PostDetailsView.as_view,
        'PUT': PostUpdateView.as_view,
        'PATCH': PostUpdateView.as_view
    }