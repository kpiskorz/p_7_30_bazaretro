from django.test import TestCase, Client
from django.core.files import File
from django.contrib.auth.models import User
from django.contrib.auth.forms import PasswordChangeForm
from .models import(
    Profile,
    Brand,
    ModelName,
    DeviceType,
    Category,
    Topic,
    Post,
    File
) 

from .serializers import(
    BrandSerializer,
    ModelNameSerializer,
    #CategorySerializer,
    DeviceTypeSerializer
) 

from .forms import(
    SignUpForm,
    AddTopic, 
    AddPost, 
    AddFile, 
    AddBrand, 
    AddModel, 
    AddType, 
    EditUserForm, 
    DeleteConfirm
)

from .views import(
    BrandDestroyView,
    BrandDetailsView,
    BrandManageView,
    BrandUpdateView,
    #CategoryDestroyView,
    #CategoryDetailsView,
    #CategoryManageView,
    #CategoryUpdateView,
    DeviceTypeDestroyView,
    DeviceTypeDetailsView,
    DeviceTypeManageView,
    DeviceTypeUpdateView,
    ModelNameDestroyView,
    ModelNameDetailsView,
    ModelNameManageView,
    ModelNameUpdateView
)

from django.urls import reverse
from unittest import mock
from rest_framework.test import APITestCase

# Create your tests here.
class UserTests(TestCase):
    def test_registerUserCorr(self):
        form_data = {
            'username' : 'Test',
            'password1' : 'P@ssw0rd!',
            'password2' : 'P@ssw0rd!',
            'email' : 'test_test@test.com'
        }
        test_form = SignUpForm(data = form_data)
        self.assertTrue(test_form.is_valid())
    def test_registerUserFalse(self):
        form_data = {
            'username' : 'Test',
            'password1' : 'P@ssw0rd!',
            'password2' : 'P@ssw0rd',
            'email' : 'test_test@test.com'
        }
        test_form = SignUpForm(data = form_data)
        self.assertFalse(test_form.is_valid())

class ForumTests(TestCase):
    def setUp(self):
        #user = Profile.objects.create(username = 'Test', email = 'test_test@test.com')
        self.cat = Category.objects.create(catName= 'Test', catDesc= 'TTT')
        self.topic = Topic(titleCat = self.cat, titleName= 'TestTestName')
    def test_addTopicTrue(self):
        form_data = {
            'titleCat' : self.cat,
            'titleName' : 'Test Title'
        }
        test_form = AddTopic(data = form_data)
        self.assertTrue(test_form.is_valid())
    def test_addTopicFalse(self):
        form_data = {
            'titleCat' : '',
            'titleName' : 'Test Title'
        }
        test_form = AddTopic(data = form_data)
        self.assertFalse(test_form.is_valid())
    def test_addPostTrue(self):
        form_data = {
            'postData' : 'Lorem Ipsum Sit Doloret Lamet'
        }
        test_form = AddPost(data = form_data)
        self.assertTrue(test_form.is_valid())
    def test_addPostFalse(self):
        form_data = {
            'postData' : ''
        }
        test_form = AddPost(data = form_data)
        self.assertFalse(test_form.is_valid())

class FileUploadTests(TestCase):
    def setUp(self):
        self.brand = Brand.objects.create(brandName= 'Test')
        self.model = ModelName.objects.create(modelName = 'ZX880', brand = self.brand)
    def test_file_field(self):
        file_mock = mock.MagicMock(spec=File)
        file_mock.name = 'sterowniki.zip'
        file_model = File(file=file_mock)
        self.assertEqual(file_model.file.name, file_mock.name)

class EditDataTest(TestCase):
    def setUp(self):
        self.brand = Brand.objects.create(pk=1, brandName= 'Test')
    def test_addBrandTrue(self):
        form_data = {
            'brandName' : 'Lorem Ipsum Sit Doloret Lamet'
        }
        test_form = AddBrand(data = form_data)
        self.assertTrue(test_form.is_valid())
    def test_addBrandFalse(self):
        form_data = {
            'brandName' : ''
        }
        test_form = AddBrand(data = form_data)
        self.assertFalse(test_form.is_valid())
    def test_addModelTrue(self):
        form_data = {
            'modelName' : 'Lorem Ipsum Sit Doloret Lamet',
            'brand' : 1
        }
        test_form = AddModel(data = form_data)
        self.assertTrue(test_form.is_valid())
    def test_addModelFalseNoModel(self):
        form_data = {
            'modelName' : '',
            'brand' : 1
        }
        test_form = AddModel(data = form_data)
        self.assertFalse(test_form.is_valid())
    def test_addModelFalseNoBrand(self):
        form_data = {
            'modelName' : 'Test',
            'brand' : ''
        }
        test_form = AddModel(data = form_data)
        self.assertFalse(test_form.is_valid())
    def test_addTypeTrue(self):
        form_data = {
            'deviceTypeName' : 'Lorem Ipsum Sit Doloret Lamet'
        }
        test_form = AddType(data = form_data)
        self.assertTrue(test_form.is_valid())
    def test_addTypeFalse(self):
        form_data = {
            'deviceTypeName' : ''
        }
        test_form = AddType(data = form_data)
        self.assertFalse(test_form.is_valid())

class deleteTests(TestCase):
    def setUp(self):
        #self.user = User.objects.create_user(username = 'Testowy', password = 'P@ssw0rd!', email = 'test_test@test.com')
        #self.profile = Profile.objects.create(pk=3, user_id=3)
        self.cat = Category.objects.create(catName= 'Test', catDesc= 'TTT')
        self.topic = Topic(pk=1, titleCat = self.cat, titleName= 'TestTestName')
        self.file_mock = mock.MagicMock(spec=File)
        self.file_mock.name = 'test.pdf'
        #self.file = File.objects.create(pk=1, fileName="Test", fileDesc="Test Description", file=self.file_mock)
        self.client = Client()
    #def test_deleteTopic(self):
    #    response = self.client.get(reverse('deleteTopic', args=(self.topic.pk,)), follow=True)
    #    self.assertRedirects(response, reverse('editUser'), status_code=302)
    #def test_deleteFile(self):
    #    response = self.client.get(reverse('deleteFile', args=(self.file.pk,)), follow=True)
    #    response = self.client.get('/retrobaza/user/edit/')
    #    self.assertEqual(response.status_code, 200)
    def test_deleteUser(self):
        form_data = {
            'userId' : 1
        }
        test_form = DeleteConfirm(data = form_data)
        response = self.client.get('/retrobaza/')
        self.assertEqual(response.status_code, 200)

class editTests(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username = 'Test', password = 'P@ssw0rd!', email = 'test_test@test.com')
        self.profile = Profile.objects.create(pk=2, user_id=2)
    def test_editLogin_Success(self):
        form_data = {
            'firstName' : 'Dolores',
            'lastName' : 'Mayflower'
        }
        test_form = EditUserForm(data = form_data)
        self.assertTrue(test_form.is_valid())
    def test_editLogin_WrongEmail(self):
        form_data = {
            'firstName' : 'Dolores',
            'lastName' : 'Mayflower',
            'email' : 'powerplant'
        }
        test_form = EditUserForm(data = form_data)
        self.assertFalse(test_form.is_valid())
    def test_editPass_Success(self):
        form_data = {
            'old_password' : 'P@ssw0rd!',
            'new_password1' : 'ZAQ!2wsx',
            'new_password2' : 'ZAQ!2wsx'
        }
        test_form = PasswordChangeForm(data = form_data, user=self.user)
        self.assertTrue(test_form.is_valid())
    def test_editPass_WrongPass(self):
        form_data = {
            'old_password' : 'P2ssw0rd!',
            'new_password1' : 'ZAQ!2wsx',
            'new_password2' : 'ZAQ!2wsx'
        }
        test_form = PasswordChangeForm(data = form_data, user=self.user )
        self.assertFalse(test_form.is_valid())
    def test_editPass_WrongNewPass(self):
        form_data = {
            'old_password' : 'P@ssw0rd!',
            'new_password1' : 'ZAQ12wsx',
            'new_password2' : 'ZAQ!2wsx'
        }
        test_form = PasswordChangeForm(data = form_data, user=self.user )
        self.assertFalse(test_form.is_valid())
    def test_editPass_NoPass(self):
        form_data = {
            'old_password' : '',
            'new_password1' : 'ZAQ!2wsx',
            'new_password2' : 'ZAQ!2wsx'
        }
        test_form = PasswordChangeForm(data = form_data, user=self.user )
        self.assertFalse(test_form.is_valid())
    def test_editPass_OnlyOnePass(self):
        form_data = {
            'old_password' : 'P@ssw0rd!',
            'new_password1' : 'ZAQ!2wsx',
            'new_password2' : ''
        }
        test_form = PasswordChangeForm(data = form_data, user=self.user )
        self.assertFalse(test_form.is_valid())

class BrandManageViewTests(APITestCase):
    def test_method_pairing(self):
        self.assertEqual(len(BrandManageView.VIEWS_BY_METHOD.keys()), 4)  # we only support 4 methods
        self.assertEqual(BrandManageView.VIEWS_BY_METHOD['DELETE'], BrandDestroyView.as_view)
        self.assertEqual(BrandManageView.VIEWS_BY_METHOD['GET'], BrandDetailsView.as_view)
        self.assertEqual(BrandManageView.VIEWS_BY_METHOD['PUT'], BrandUpdateView.as_view)
        self.assertEqual(BrandManageView.VIEWS_BY_METHOD['PATCH'], BrandUpdateView.as_view)

    def test_non_supported_method_returns_405(self):
        brand = Brand.objects.create(brandName='Apple', brandSlug='apple')
        response = self.client.post(f'/retrobaza/brands_api/{brand.id}')
        self.assertEqual(response.status_code, 405)
'''
class CategoryManageViewTests(APITestCase):
    def test_method_pairing(self):
        self.assertEqual(len(CategoryManageView.VIEWS_BY_METHOD.keys()), 4)  # we only support 4 methods
        self.assertEqual(CategoryManageView.VIEWS_BY_METHOD['DELETE'], CategoryDestroyView.as_view)
        self.assertEqual(CategoryManageView.VIEWS_BY_METHOD['GET'], CategoryDetailsView.as_view)
        self.assertEqual(CategoryManageView.VIEWS_BY_METHOD['PUT'], CategoryUpdateView.as_view)
        self.assertEqual(CategoryManageView.VIEWS_BY_METHOD['PATCH'], CategoryUpdateView.as_view)

    def test_non_supported_method_returns_405(self):
        brand = Category.objects.create(catName='Apple', catDesc='apple', catSlug='apple')
        response = self.client.post(f'/retrobaza/brands_api/{brand.id}')
        self.assertEqual(response.status_code, 405)
'''
class ModelNameManageViewTests(APITestCase):
    def test_method_pairing(self):
        self.assertEqual(len(ModelNameManageView.VIEWS_BY_METHOD.keys()), 4)  # we only support 4 methods
        self.assertEqual(ModelNameManageView.VIEWS_BY_METHOD['DELETE'], ModelNameDestroyView.as_view)
        self.assertEqual(ModelNameManageView.VIEWS_BY_METHOD['GET'], ModelNameDetailsView.as_view)
        self.assertEqual(ModelNameManageView.VIEWS_BY_METHOD['PUT'], ModelNameUpdateView.as_view)
        self.assertEqual(ModelNameManageView.VIEWS_BY_METHOD['PATCH'], ModelNameUpdateView.as_view)

    def test_non_supported_method_returns_405(self):
        brand = Brand.objects.create(brandName='Apple', brandSlug='apple')
        modelname = ModelName.objects.create(modelName='II SE', brand=brand, modelSlug='ii_se')
        response = self.client.post(f'/retrobaza/models_api/{modelname.id}')
        self.assertEqual(response.status_code, 405)

class DeviceTypeManageViewTests(APITestCase):
    def test_method_pairing(self):
        self.assertEqual(len(DeviceTypeManageView.VIEWS_BY_METHOD.keys()), 4)  # we only support 4 methods
        self.assertEqual(DeviceTypeManageView.VIEWS_BY_METHOD['DELETE'], DeviceTypeDestroyView.as_view)
        self.assertEqual(DeviceTypeManageView.VIEWS_BY_METHOD['GET'], DeviceTypeDetailsView.as_view)
        self.assertEqual(DeviceTypeManageView.VIEWS_BY_METHOD['PUT'], DeviceTypeUpdateView.as_view)
        self.assertEqual(DeviceTypeManageView.VIEWS_BY_METHOD['PATCH'], DeviceTypeUpdateView.as_view)

    def test_non_supported_method_returns_405(self):
        devicetype = DeviceType.objects.create(deviceTypeName='Karty sieciowe', deviceSlug='karty_sieciowe')
        response = self.client.post(f'/retrobaza/devicetypes_api/{devicetype.id}')
        self.assertEqual(response.status_code, 405)