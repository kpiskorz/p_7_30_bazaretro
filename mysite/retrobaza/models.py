from django.db import models
from django.conf import settings
from django.utils import timezone
from django.contrib.auth.models import User
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
from django.template.defaultfilters import slugify


#Model of user (needed - added avatar field)
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = models.ImageField(upload_to='user_avatars/', blank=True, null=True)
    is_blocked = models.BooleanField(default=False)
    @receiver(post_save, sender=User)
    def update_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)
        instance.profile.save()
    def __str__(self):
        return self.user.username
    def delete(self, *args, **kwargs):
        self.avatar.delete()
        self.user.delete()
        super().delete(*args, **kwargs)

#Model of brand
class Brand(models.Model):
    brandName = models.CharField(max_length=50)
    brandSlug = models.SlugField()
    def save(self, *args, **kwargs):
        if not self.id:
            # Newly created object, so set slug
            self.brandSlug = slugify(self.brandName)

        super(Brand, self).save(*args, **kwargs)
    def __str__(self):
        return self.brandName

#Model of model name
class ModelName(models.Model):
    modelName = models.CharField(max_length=50)
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE)
    modelSlug = models.SlugField()
    def save(self, *args, **kwargs):
        if not self.id:
            # Newly created object, so set slug
            self.modelSlug = slugify(self.modelName)

        super(ModelName, self).save(*args, **kwargs)
    def __str__(self):
        return self.modelName

#Model of device Type
class DeviceType(models.Model):
    deviceTypeName = models.CharField(max_length=50)
    deviceSlug = models.SlugField()
    def save(self, *args, **kwargs):
        if not self.id:
            # Newly created object, so set slug
            self.deviceSlug = slugify(self.deviceTypeName)

        super(DeviceType, self).save(*args, **kwargs)
    def __str__(self):
        return self.deviceTypeName

### Model of file (Commented due to the unknown interpretation of file upload scheme)

def user_directory_path(instance, filename):
    return '{0}/{1}/{2}'.format(instance.model.brand.brandSlug, instance.model.modelSlug, filename)

class File(models.Model):
    fileName = models.CharField(max_length=200)
    fileDesc = models.TextField()
    uploader = models.ForeignKey(Profile,null=True, on_delete=models.SET_NULL)
    brand = models.ForeignKey(Brand, null=True, on_delete=models.SET_NULL)
    model = models.ForeignKey(ModelName,null=True, on_delete=models.SET_NULL)
    deviceType = models.ForeignKey(DeviceType,null=True, on_delete=models.SET_NULL)
    file = models.FileField(upload_to=user_directory_path )
    def delete(self, *args, **kwargs):
        self.file.delete()
        super().delete(*args, **kwargs)


#Model of forum category
class Category(models.Model):
    catName = models.CharField(max_length=50)
    catDesc = models.CharField(max_length=255)
    catPic = models.ImageField(upload_to='forum_cats')
    catSlug =  models.SlugField()
    def save(self, *args, **kwargs):
        if not self.id:
            # Newly created object, so set slug
            self.catSlug = slugify(self.catName)

        super(Category, self).save(*args, **kwargs)
    def __str__(self):
        return self.catName

#Model of forum topic
class Topic(models.Model):
    author = models.ForeignKey(Profile,null=True, on_delete=models.SET_NULL)
    titleCat = models.ForeignKey(Category, on_delete=models.CASCADE)
    titleName = models.CharField(max_length=200)
    created_date = models.DateTimeField(default=timezone.now)
    titleSlug =  models.SlugField()
    def save(self, *args, **kwargs):
        if not self.id:
            # Newly created object, so set slug
            self.titleSlug = slugify(self.titleName)

        super(Topic, self).save(*args, **kwargs)
    def __str__(self):
        return self.titleName

#Model of forum post
class Post(models.Model):
    author = models.ForeignKey(Profile,null=True, on_delete=models.SET_NULL)
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE)
    postData = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)